﻿namespace CodeGenerator4.Classes
{
    public class GenFileInfo
    {
        public string TableName { get; set; }
        public string ProjectName { get; set; }
        public string FileType { get; set; }
        public string InputFile { get; set; }
        public string OutputFile { get; set; }
        public bool CopyToProjectFolder { get; set; }
        public bool AddToProject { get; set; }
        public bool DefaultChecked { get; set; }
        public string Group { get; set; }  
    }
}
