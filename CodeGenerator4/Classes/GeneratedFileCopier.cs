﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using EnvDTE;
using EnvDTE80;

namespace CodeGenerator4.Classes
{
    public class GeneratedFileCopier
    {
        public static void CopyAllFiles(List<GenFileInfo> genFileInfos)
        {
            foreach (var genFileInfo in genFileInfos)
            {
                if (genFileInfo.CopyToProjectFolder == true)
                {
                    CopyFile(genFileInfo);
                }
            }
        }

        public static void CopyFile(GenFileInfo genFileInfo)
        {
            if (System.IO.File.Exists(genFileInfo.InputFile))
            {
                if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(genFileInfo.OutputFile)))
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(genFileInfo.OutputFile));
                }

                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(genFileInfo.OutputFile)))
                {
                    if (genFileInfo.CopyToProjectFolder)
                    {
                        System.IO.File.Copy(genFileInfo.InputFile, genFileInfo.OutputFile, true);
                    }
                }
                else
                {
                    throw new ApplicationException(string.Format("Output File not found  {0}", genFileInfo.OutputFile));
                }
            }
            else
            {
                throw new ApplicationException(string.Format("Input File not found  {0}", genFileInfo.InputFile));
            }
        }

        public static List<string> LoadTableNames(DTE2 applicationObject)
        {
            List<string> tableInfoList = new List<string>();

            var generatedFiles = LoadXmlDocumentGeneratedFiles(applicationObject);

            XmlNodeList tables = generatedFiles.SelectNodes("entities/entity");
            foreach (XmlNode table in tables)
            {

                XmlAttribute tablenameAttribute = table.Attributes["tablename"];
                var tableName = tablenameAttribute.InnerText.Trim();
                tableInfoList.Add(tableName);
            }

            return tableInfoList;
        }


        public static List<GenFileInfo> LoadFileList(DTE2 applicationObject)
        {

            List<GenFileInfo> fileInfoList = new List<GenFileInfo>();

            var generatedFiles = LoadXmlDocumentGeneratedFiles(applicationObject);

            XmlNodeList files = generatedFiles.SelectNodes("entities/entity/files/file");
            foreach (XmlNode file in files)
            {
                XmlAttribute tablenameAttribute = file.Attributes["tablename"];
                XmlAttribute fileAttribute = file.Attributes["name"];
                XmlAttribute projectNameAttribute = file.Attributes["projectname"];
                XmlAttribute defaultCheckedAttribute = file.Attributes["defaultchecked"];
                XmlAttribute groupAttribute = file.Attributes["group"];
                XmlNode inputFile = file.SelectSingleNode("inputfile");
                XmlNode outputFile = file.SelectSingleNode("outputfile");
                GenFileInfo fileInfo = new GenFileInfo();
                fileInfo.TableName = tablenameAttribute.InnerText.Trim();
                fileInfo.FileType = fileAttribute.InnerText.Trim();
                fileInfo.Group = groupAttribute.InnerText.Trim();
                fileInfo.ProjectName = projectNameAttribute.InnerText.Trim();
                fileInfo.DefaultChecked = bool.Parse(defaultCheckedAttribute.InnerText.Trim());
                fileInfo.InputFile = inputFile.InnerText.Trim();
                fileInfo.OutputFile = outputFile.InnerText.Trim();
                if (System.IO.File.Exists(fileInfo.InputFile))
                {
                    fileInfoList.Add(fileInfo);
                }
                else
                {
                    //throw new ApplicationException(string.Format("Input File not found on startup  {0}", fileInfo.InputFile));
                    MessageBox.Show(string.Format("Input File not found on startup  {0}", fileInfo.InputFile));
                }
            }

            applicationObject.StatusBar.Progress(true, "CodeGen-Loading Tables", 5, GenerateCode.ProgressComplete);
            System.Windows.Forms.Application.DoEvents();

            return fileInfoList;
        }


        private static XmlDocument LoadXmlDocumentGeneratedFiles(DTE2 applicationObject)
        {
            Project project = SolutionAdder.GetProject(applicationObject, "GeneratorTemplates");
            string fileName = Path.GetDirectoryName(project.FullName) + @"\FileLocations.xml";

            XmlDocument generatedFiles = null;
            generatedFiles = new XmlDocument();
            generatedFiles.Load(fileName);
            return generatedFiles;
        }
    }
}
