﻿using System;
using EnvDTE;
using EnvDTE80;

namespace CodeGenerator4.Classes
{
    public class OutputWindowLogger
    {
        private string eventSource = string.Empty;
        private OutputWindowPane outputWindowPane = null;
        private bool logtoOutputWindow = true;

        public OutputWindowLogger(DTE2 _applicationObject, string _eventSource)
        {
            try
            {
                eventSource = _eventSource;

                // Create an output pane for this addin
                Window window = _applicationObject.Windows.Item(Constants.vsWindowKindOutput);
                OutputWindow outputWindow = (OutputWindow)window.Object;
                outputWindowPane = null;

                for (int i = 1; i <= outputWindow.OutputWindowPanes.Count; ++i)
                {
                    if (outputWindow.OutputWindowPanes.Item(i).Name.Equals(eventSource,
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        outputWindowPane = outputWindow.OutputWindowPanes.Item(i);
                        break;
                    }
                }

                if (outputWindowPane == null)
                    outputWindowPane = outputWindow.OutputWindowPanes.Add(eventSource);

            }
            catch
            {
                // Swallow it, never let errors in logging stop the add in
            }
        }

        /// <summary>
        /// Log trace
        /// </summary>
        /// <param name="message"></param>
        public void LogMessage(string message)
        {
            try
            {
                outputWindowPane.OutputString(string.Format("{0}\n", message));
            }
            catch
            {
                // Swallow, never let errors in logging stop the add in
            }
        }

        /// <summary>
        /// Log an error
        /// </summary>
        /// <param name="message"></param>
        public void LogError(string message)
        {
            try
            {
                outputWindowPane.OutputString(string.Format("Error: {0}\n", message));
            }
            catch
            {
                // Swallow, never let errors in logging stop the add in
            }
        }
    }
}
