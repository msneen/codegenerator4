﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeGenerator4.Classes;
using EnvDTE;
using EnvDTE80;

namespace CodeGenerator4.Classes
{
    public class SolutionAdder
    {
        //public static void AddFiles(DTE2 applicationObject, List<GenFileInfo> genFileInfos)
        //{
        //    foreach (var genFileInfo in genFileInfos)
        //    {
        //        if (genFileInfo.AddToProject == true)
        //        {
        //            Project recoProject = GetProject(applicationObject, genFileInfo.ProjectName);
        //            if (recoProject != null && genFileInfo.CopyToProjectFolder)
        //            {
        //                recoProject.ProjectItems.AddFromFile(genFileInfo.OutputFile);
        //            }
        //        }
        //    }
        //}

        public static Project GetProject(DTE2 applicationObject, string projectName)
        {
            Project genProject = null;

            var item = applicationObject.Solution.Projects.GetEnumerator();

            while (item.MoveNext())
            {

                genProject = item.Current as Project;
                if (genProject != null)
                {
                    System.Diagnostics.Debug.WriteLine(genProject.Name);

                    var projectItems = genProject.ProjectItems.GetEnumerator();
                    while (projectItems.MoveNext())
                    {
                        ProjectItem projectItem = projectItems.Current as ProjectItem;
                        if (projectItem != null)
                        {
                            System.Diagnostics.Debug.WriteLine("-> " + projectItem.Name);
                            if (projectItem.Name == projectName)
                            {
                                genProject = projectItem.SubProject as Project;
                                if (genProject != null)
                                {
                                    return genProject;
                                }
                            }
                        }
                    }

                    if (genProject.Name == projectName)
                    {
                        return genProject;
                    }
                }
            }

            return null;
        }
    }
}
