﻿//------------------------------------------------------------------------------
// <copyright file="GenerateCode.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.Globalization;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using CodeGenerator4.Classes;
using System.Collections.Generic;
using CodeGenerator4.Popups;
using System.Windows.Forms;

namespace CodeGenerator4
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class GenerateCode
    {
        public static int ProgressComplete = 30;
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("c275e3d9-d13a-4c55-97d9-affeccb2ef89");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Package package;



        /// <summary>
        /// Initializes a new instance of the <see cref="GenerateCode"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private GenerateCode(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package");
            }

            this.package = package;


            OleMenuCommandService commandService = this.ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new MenuCommand(this.MenuItemCallback, menuCommandID);
                commandService.AddCommand(menuItem);
            }
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static GenerateCode Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private IServiceProvider ServiceProvider
        {
            get
            {
                return this.package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            //var _applicationObject = (DTE2)GetService(typeof(DTE));
            Instance = new GenerateCode(package);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            var _applicationObject = (EnvDTE80.DTE2)this.ServiceProvider.GetService(typeof(EnvDTE.DTE));
            try {

                List<string> tables = GeneratedFileCopier.LoadTableNames(_applicationObject);

                List<GenFileInfo> genFileInfos = GeneratedFileCopier.LoadFileList(_applicationObject);

                using (var fileList = new FileList(_applicationObject, genFileInfos, tables))
                {
                    if (fileList.ShowDialog() == DialogResult.OK)   //10
                    {
                        genFileInfos = fileList.SelectedFileInfos;

                        _applicationObject.StatusBar.Progress(true, "CodeGen-Starting File Copy", 10, ProgressComplete);
                        System.Windows.Forms.Application.DoEvents();

                        GeneratedFileCopier.CopyAllFiles(genFileInfos);
                        ////21 copy the files to the project folders
                        _applicationObject.StatusBar.Progress(true, "CodeGen- File Copy Complete", 30, ProgressComplete);
                        System.Windows.Forms.Application.DoEvents();
                    }
                }
            }
            catch (Exception ex)
            {
                OutputWindowLogger Logger = new OutputWindowLogger(_applicationObject, "GeneratorControl");
                Logger.LogError("Generic Error in Addin " + ex.Message);
            }
        }
    }
}
