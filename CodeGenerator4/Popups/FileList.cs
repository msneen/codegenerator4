﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Windows.Forms;
using CodeGenerator4.Classes;
using EnvDTE80;

namespace CodeGenerator4.Popups
{
    public partial class FileList : Form
    {
        public List<GenFileInfo> GenFileInfos { get; set; }
        public List<GenFileInfo> SelectedFileInfos { get; set; }
        private DTE2 _applicationObject;

        private List<GenCheckBox> copyToProjectDirectries = new List<GenCheckBox>();
        private List<GenCheckBox> addToProjects = new List<GenCheckBox>();
        private OutputWindowLogger Logger = null;

        public List<string> TableNames { get; set; }

        public FileList(DTE2 applicationObject, List<GenFileInfo> genFileInfos, List<string> tableNames )
        {
            GenFileInfos = genFileInfos;
            TableNames = tableNames;
            _applicationObject = applicationObject;
            try
            {
                Logger = new OutputWindowLogger(_applicationObject, "GeneratorControl");
            }
            catch
            {

            }
            InitializeComponent();
        }

        private void FileList_Load(object sender, EventArgs e)
        {
            cboTables.Items.Clear();
            foreach (var tableName in TableNames)
            {
                cboTables.Items.Add(tableName);
            }
        }
        //[PrincipalPermission(SecurityAction.Demand, Role = @"BUILTIN\Administrators")]
        private void LoadFiles(string tableName)
        {
            
            try
            {
                var group = "";
                var rowNumber = 0;

                tableLayoutPanel1.Visible = false;

                var genFileInfos = GenFileInfos.Where(e => e.TableName == tableName).ToList();

                tableLayoutPanel1.Controls.Clear();
                tableLayoutPanel1.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
                tableLayoutPanel1.RowCount = genFileInfos.Count;
                foreach (RowStyle style in tableLayoutPanel1.RowStyles)
                {
                    style.SizeType = SizeType.Absolute;
                    style.Height = 80;
                }

                for (var i = 0; i < genFileInfos.Count; i++)
                {
                    if (genFileInfos[i].Group != group)
                    {
                        group = genFileInfos[i].Group;
                        var separator = new Label()
                        {
                            Height = 50,
                            Text = group,
                            BackColor = Color.LightGray,
                            Dock = DockStyle.Fill
                        };
                        tableLayoutPanel1.SetColumnSpan(separator, 4);
                        tableLayoutPanel1.Controls.Add(separator, 0, rowNumber);
                        //tableLayoutPanel1.RowStyles[rowNumber].Height = 35;
                        rowNumber++;
                    }
                    genFileInfos[i].CopyToProjectFolder = genFileInfos[i].DefaultChecked;
                    genFileInfos[i].AddToProject = genFileInfos[i].DefaultChecked;

                    GenCheckBox copyToProjectFolder = new GenCheckBox()
                    {
                        Checked = genFileInfos[i].DefaultChecked,
                        GenFileInfo = genFileInfos[i],
                        Height = 50

                    };
                    copyToProjectDirectries.Add(copyToProjectFolder);
                    copyToProjectFolder.CheckedChanged += delegate(object o, EventArgs args)
                    {
                        ((GenCheckBox) o).GenFileInfo.CopyToProjectFolder = ((GenCheckBox) o).Checked;
                        ((GenCheckBox) o).GenFileInfo.AddToProject = ((GenCheckBox) o).Checked;
                    };
                    SetToolTip(copyToProjectFolder, "Check to copy this file to project file when you click 'OK'");
                    tableLayoutPanel1.Controls.Add(copyToProjectFolder, 0, rowNumber);


                    GenButton diffButton = new GenButton()
                    {
                        Text = "Diff",
                        Font = new Font("Serif", 8),
                        Height = 50,
                        GenFileInfo = genFileInfos[i]
                    };
                    diffButton.Click += delegate(object o, EventArgs args)
                    {
                        try
                        {
                            FileInfo outPutfile = new FileInfo(((GenButton) o).GenFileInfo.OutputFile);
                            outPutfile.IsReadOnly = false;
                        }
                        catch (Exception) { }
                        //Launch KDiff
                        string kDiffPath = @"C:\Program Files (x86)\KDiff3\Kdiff3.exe";
                        if (!File.Exists(kDiffPath))
                        {
                            kDiffPath = @"C:\Program Files\KDiff3\Kdiff3.exe";
                        }

                        ProcessStartInfo startInfo = new ProcessStartInfo(kDiffPath);
                        startInfo.Arguments = String.Format("\"{0}\" \"{1}\" -o \"{1}\"",
                            ((GenButton) o).GenFileInfo.InputFile,
                            ((GenButton) o).GenFileInfo.OutputFile);
                        startInfo.Verb = "runas";
                        startInfo.UseShellExecute = true;

                        System.Diagnostics.Process.Start(startInfo);
                        //kDiffPath = @"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\vsdiffmerge.exe";

                    };
                    tableLayoutPanel1.Controls.Add(diffButton, 1, rowNumber);

                    tableLayoutPanel1.Controls.Add(
                        new Label() {Width = 500, Height = 80, Text = Path.GetFileName(genFileInfos[i].OutputFile)}, 2, rowNumber
                        );
                    
                    rowNumber++;

                    SelectedFileInfos = genFileInfos;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(string.Format("{0} - {1}", ex.Message, ex.StackTrace));
            }
            tableLayoutPanel1.Visible = true;
        }

        private void SetToolTip(Control control, string message)
        {
            try
            {
                // Create the ToolTip and associate with the Form container.
                ToolTip toolTip1 = new ToolTip();

                // Set up the delays for the ToolTip.
                toolTip1.AutoPopDelay = 5000;
                toolTip1.InitialDelay = 1000;
                toolTip1.ReshowDelay = 500;
                // Force the ToolTip text to be displayed whether or not the form is active.
                toolTip1.ShowAlways = true;

                // Set up the ToolTip text for the control
                toolTip1.SetToolTip(control, message);
            }
            catch (Exception ex)
            {
                Logger.LogError(string.Format("{0} - {1}", ex.Message, ex.StackTrace));
            }
        }

        private void chkCopyAllNone_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (var copyToCheckbox in copyToProjectDirectries)
                {
                    copyToCheckbox.Checked = chkCopyAllNone.Checked;
                }

                foreach (var addToCheckbox in addToProjects)
                {
                    addToCheckbox.Checked = chkCopyAllNone.Checked;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(string.Format("{0} - {1}", ex.Message, ex.StackTrace));
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void cboTables_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadFiles(cboTables.SelectedItem.ToString());
        }
    }

    public class GenCheckBox : CheckBox
    {
        public GenFileInfo GenFileInfo { get; set; }
    }

    public class GenButton : Button
    {
        public GenFileInfo GenFileInfo { get; set; }
    }
}
